from django.db import models

    
class Class(models.Model):
    name      = models.CharField(max_length = 5)

    def __str__(self):
        return self.name

class Student(models.Model):
    name         = models.CharField(max_length = 30)
    surname      = models.CharField(max_length = 30)
    password     = models.CharField(max_length = 20)
    classes      = models.ForeignKey(Class, on_delete = models.CASCADE)
    
    def __str__ (self):
        return self.name + " " + self.surname

class Subject(models.Model):
    name = models.CharField(max_length = 30)

    def __str__ (self):
        return self.name

        
class Mark(models.Model):
    student   = models.ForeignKey(Student, on_delete = models.CASCADE)
    subject   = models.ForeignKey(Subject, on_delete = models.CASCADE)
    mark      = models.IntegerField(default = 0)
    date      = models.DateTimeField('date')
    explain   = models.CharField(max_length = 50)

    def __str__ (self):
        return "{0} получил {1} по предмету {2}".format(self.student, self.mark, self.subject)

    
