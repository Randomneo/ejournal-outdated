from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import loader

from .models import Student, Mark, Subject, Class

def index(request):
    students = Student.objects.all()
    subjects = Subject.objects.all()
    classes  = Class.objects.all()
    template = loader.get_template('journal/index.html')
    context = {
        'students' : students,
        'subjects' : subjects,
        'classes'  : classes,
    }
    return HttpResponse(template.render(context, request))

def wrong_pass(request):
    template = loader.get_template('journal/wrong_pass.html')
    return HttpResponse(template.render({}, request))
    
def detail_all(request):
    _student_id = request.POST['student']
    _password   = request.POST['password']

    marks = Mark.objects.filter(student_id = _student_id);
    student = marks[0].student
    subjects= Subject.objects.all()
    marks = reversed(marks)
    
    if _password != student.password:
        return wrong_pass(request)
    
    template = loader.get_template('journal/detail_all.html')
    context  = {
        "student": student,
        "subjects": subjects,
        "marks": marks,
    }
    
    return HttpResponse(template.render(context, request))

def detail(request):
    _subject_id = request.POST['subject']
    if _subject_id == "-1":
        return detail_all(request)

    _student_id = request.POST['student']
    _password   = request.POST['password']
    #_class_id   = request.POST['class']

    marks = Mark.objects.filter(student_id = _student_id, subject_id = _subject_id);
    student = marks[0].student
    subjcet = marks[0].subject    
    marks = reversed(marks)
    
    if _password != student.password:
        return wrong_pass(request)
    
    template = loader.get_template('journal/detail.html')
    context = {
        "marks": marks,
        "student": student,
        "subject": subjcet,
    }
    return HttpResponse(template.render(context, request))

