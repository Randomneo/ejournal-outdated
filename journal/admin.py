from django.contrib import admin

from .models import Student, Subject, Mark, Class

class MarkInline(admin.StackedInline):
    model = Mark
    extra = 0

class StudentAdmin(admin.ModelAdmin):
    inlines = [MarkInline]

admin.site.register(Student, StudentAdmin)
admin.site.register(Mark)
admin.site.register(Subject)
admin.site.register(Class)
